import datetime
import asyncio
import pickle
import time
import uuid
import os

from googleapiclient.discovery_cache.base import Cache
from ruamel import yaml

from discord.ext import commands
import discord
Cog = commands.Cog

import googleapiclient.discovery
import google_auth_oauthlib.flow
import google.auth.transport.requests


cog = None

# TODO: YAML
config = {
    290573725366091787: {
        'leave_c': 290757101914030080,
        'leave_r': [320939806530076673],

        'log_c': 319830765842071563,

        'jail': {
            'channel': 365892375492427778,
            'role': 320939806530076673,
            'respond': True,
            'black_roles': [290750253102268427],
            'extra_msg': 'o/'
            }
    },
    184755239952318464: {
        'leave_c': 207659596167249920,  # joinbot
        'leave_r': [
            334301546710040576,  # Memelord
            388886242458075137,  # Muted
        ],

        'log_c':   422185677354958858,  # messagebot

        'jail': {
            'channel': 334296605349904384,  # snail-time
            'role':    334301546710040576,  # Memelord
            'respond': True,
            'black_roles': [
                397130936464048129,  # Spoilers
                392842859931369483,  # Vote spoilser
                392847202575187970,  # SADAMA
            ],
            'extra_msg': 'If you continue to break rules in here, you may be banned immediately.\n'
                         'If you leave the server during this timeframe, you *will* be banned immediately.'
        }
    },
    282219466589208576: {
        'leave_c': 282477076454309888,  # bote-spam-but-its-a-bot
        'leave_r': [
            282647198322393098,  # Muted
        ],

        'log_c':   451134620612689920,  # edits-and-deletes

        'jail': {
            'channel': 282647369961570305,  # ask-about-your-mute
            'role':    282647198322393098,  # Muted
            'respond': True,
            'black_roles': [],
            'extra_msg': 'If you leave the server during this timeframe, you *will* be banned immediately.'
        }
    }
}


SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
CLIENT_SECRET_FILE = 'config/secret_2.json'
TOKEN_FILE = 'state_cache/client_token.pickle'

SPREADSHEET_ID = '1yNx74RcSOLDaMOc2p-I5Nsr4lWMNQdpO1NBseJU0wQo'
SHEET_RANGE = 'A15:G'

OFFENCE_TIERS = {
    0: 'Note',
    1: '1. Warning',
    2: '2. Warning 2',
    3: '3. Memelord [1h]',
    4: '4. Final Memelord [2h]',
    5: 'BANNED',
}


class MemoryCache(Cache):
    """
    A modified version of  https://github.com/googleapis/google-api-python-client/issues/325#issuecomment-274349841
    """
    _CACHE = {}

    @classmethod
    def get(cls, url):
        return cls._CACHE.get(url)

    @classmethod
    def set(cls, url, content):
        cls._CACHE[url] = content


class Offence:
    def __init__(self, details):
        while len(details) < 7:
            details.append('')
        self.id, self.username, self.level, self.note, self.reason, self.moderator, self.date = details

        for i in OFFENCE_TIERS:
            if OFFENCE_TIERS[i] == self.level:
                self.level = i
                break
        else:
            self.level = 0

    def __str__(self):
        return '{1}: {0.username} (<@{0.id}>): {0.reason} | {0.moderator}'.format(
            self, OFFENCE_TIERS.get(self.level))

    def __repr__(self):
        return '<{}>'.format(self)

    def __radd__(self, other):
        return self + other

    def __add__(self, other):
        if isinstance(other, int):
            return max(self.level, other)
        return max(self.level, other.level)


class Moderation(Cog):
    # Preserve between reloads?
    sheets_service = None

    WARNING_RATELIMIT = 60 * 4  # Seconds

    def __init__(self, bot):
        super().__init__()

        self.bot = bot
        try:
            with open('state_cache/in_jail.yml', 'r') as file_:
                self.in_jail = yaml.load(file_, Loader=yaml.Loader)
        except FileNotFoundError:
            self.in_jail = [
                # [guild, member, time, ending, reason, uuid]
            ]

        self.config = config

        self.banned_users = {}
        self.timer = self.bot.loop.create_task(self.check_timer())

        self.slowmode = {}  # channel: seconds
        self.last_sent = {}  # channel: {member: datetime}

        self.warnings = {}

        self.ensure_sheets_service()

    async def check_timer(self):
        await self.bot.wait_until_ready()

        while True:
            for convict in list(self.in_jail):
                if convict[3] is not None and time.time() > convict[3]:
                    guild = self.bot.get_guild(convict[0])

                    if guild is None:
                        self.in_jail.remove(convict)
                        continue

                    member = guild.get_member(convict[1])
                    if member is None:
                        self.in_jail.remove(convict)
                        continue

                    if guild.id not in self.config or self.config[guild.id].get('jail', {}).get('role') is None:
                        self.in_jail.remove(convict)
                        continue

                    channel = guild.get_channel(self.config[guild.id]['jail']['channel'])
                    try:
                        await member.remove_roles(discord.Object(self.config[guild.id]['jail']['role']))
                    except Exception as e:
                        if channel:
                            await channel.send(f'Error removing role: {e}')
                    else:
                        if channel:
                            await channel.send(f'{member} was released')

                    self.in_jail.remove(convict)

                    await self.save(None)
            await asyncio.sleep(5)

    @Cog.listener()
    async def on_message(self, message):
        if isinstance(message.author, discord.Member):
            is_mod = message.channel.permissions_for(message.author).manage_messages
        else:
            is_mod = False
        cid = message.channel.id
        aid = message.author.id

        if is_mod:
            # Keep it simple
            return

        # Ratelimit non-moderator messages
        if cid in self.slowmode and not is_mod:
            if cid not in self.last_sent:
                self.last_sent[cid] = {}

            # Find their last messages
            last = self.last_sent[cid].get(aid)

        mentions = map(lambda m: m.id, message.mentions)
        if (154825973278310400 in mentions or 181452328073822208 in mentions) and 'rape' in message.content.lower():
            await message.author.ban(delete_message_days=1, reason='Harassment')

        if message.channel.id in self.slowmode:
            if message.channel.id not in self.last_sent:
                self.last_sent[message.channel.id] = {}
            last = self.last_sent[message.channel.id].get(message.author.id)
            if last is None:
                # This is the first recorded message
                self.last_sent[cid][aid] = message.created_at
            else:
                # Check if we need to delete it
                rl = self.slowmode[cid]
                if (message.created_at - last).total_seconds() < rl:
                    await message.delete()
                    return  # Stop handing the message beacuse it doesn't exist
                else:
                    self.last_sent[cid][aid] = message.created_at

        # Handle mutes
        if message.guild is None or message.guild.id not in self.config: return
        jail = self.config[message.guild.id].get('jail')
        if not jail: return

        if not jail['respond']: return
        if message.channel.id != jail['channel']: return

        for convict in self.in_jail:
            if convict[0] != message.guild.id or convict[3]: continue
            if convict[1] != message.author.id: continue

            length = convict[2]
            convict[3] = time.time() + (length * 60)
            reason = convict[4]

            msg = f'{message.author.mention}, you have been confined for {length} minutes'
            if reason:
                msg += f' because:\n**{reason}**'
            else:
                msg += '.'
            if 'extra_msg' in jail:
                msg += '\n' + jail['extra_msg']

            await message.channel.send(msg)
            await self.save(None)

    @Cog.listener()
    async def on_message_edit(self, old, message):
        if message.guild is None or message.guild.id not in self.config: return
        channel = message.guild.get_channel(self.config[message.guild.id].get('log_c', 0))
        if channel is None: return

        if old.content == message.content: return
        if message.channel.id == channel.id: return

        embed = discord.Embed(title=f'Message edited in #{message.channel.name}',
                              colour=0xff7f00,
                              description=message.content,
                              timestamp=old.created_at)
        if old.content:
            embed.add_field(name='Old content:', value=old.content[:1024])
        embed.set_author(name=message.author.name, icon_url=message.author.avatar_url_as(format='png'))
        await channel.send(embed=embed)

    @Cog.listener()
    async def on_message_delete(self, message):
        if message.guild is None or message.guild.id not in self.config: return
        channel = message.guild.get_channel(self.config[message.guild.id].get('log_c', 0))
        if channel is None: return

        if message.channel.id == channel.id: return

        embed = discord.Embed(title=f'Message deleted in #{message.channel.name}',
                              colour=0xff0000,
                              description=message.content,
                              timestamp=message.created_at)
        embed.set_author(name=message.author.name, icon_url=message.author.avatar_url_as(format='png'))
        await channel.send(embed=embed)

    @Cog.listener()
    async def on_bulk_message_delete(self, messages):
        for message in messages:
            await self.on_message_delete(message)

    @Cog.listener()
    async def on_member_ban(self, guild, member):
        self.banned_users[guild.id] = member.id

    @Cog.listener()
    async def on_member_join(self, member):
        for convict in self.in_jail:
            if member.id == convict[1]:
                if member.guild.id == convict[0]:
                    jail = self.config.get(member.guild.id, {}).get('jail', {})
                    await member.add_roles(jail.get('role', 0))

    @Cog.listener()
    async def on_member_remove(self, member):
        # Wait for the ban event to fire (if at all)
        await asyncio.sleep(0.25)
        if member.guild.id in self.banned_users and \
         member.id == self.banned_users[member.guild.id]:
            del self.banned_users[member.guild.id]
            return

        roles = self.config.get(member.guild.id, {}).get('leave_r', [])
        c_id = self.config.get(member.guild.id, {}).get('leave_c', 0)

        channel = member.guild.get_channel(c_id)
        if channel is None: return

        for i in member.roles:
            if i.id in roles:
                return await channel.send(
                    f':rotating_light::rotating_light: **{member} left with role {i}!** :rotating_light::rotating_light:'
                )

    def ensure_sheets_service(self):
        if self.sheets_service is not None:
            return

        credentials = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists(TOKEN_FILE):
            with open(TOKEN_FILE, 'rb') as token:
                credentials = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not credentials or not credentials.valid:
            if credentials and credentials.expired and credentials.refresh_token:
                credentials.refresh(google.auth.transport.requests.Request())
            else:
                flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(
                    CLIENT_SECRET_FILE, SCOPES)
                credentials = flow.run_local_server()
            # Save the credentials for the next run
            with open(TOKEN_FILE, 'wb') as token:
                pickle.dump(credentials, token)

        self.sheets_service = googleapiclient.discovery.build('sheets', 'v4', credentials=credentials,
                                                              cache=MemoryCache)

    def get_offences_for(self, user_id=None, username=None):
        self.ensure_sheets_service()

        sheet = self.sheets_service.spreadsheets()
        result = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                                    range=SHEET_RANGE).execute()
        values = result.get('values', [])

        hits = [
            Offence(i) for i in values if ((i[0] == user_id or user_id is None) and
                                           (i[1] == '@' + str(username) or username is None))
        ]

        return hits

    def add_new_offence(self, user_id, username, tier, reason, moderator, notes=''):
        user_id = str(user_id)

        def _add_new_offence():
            offences = self.get_offences_for(user_id)

            current_level = sum(offences)
            new_level = min(current_level + tier, 5)  # Cap at BANNED

            date = datetime.datetime.now().replace(microsecond=0).isoformat()
            row = [user_id, '@' + str(username), OFFENCE_TIERS.get(new_level, ''), reason, moderator, notes, date]

            sheet = self.sheets_service.spreadsheets()
            value_range_body = {
                'values': [row]
            }
            result = sheet.values().append(spreadsheetId=SPREADSHEET_ID,
                                           range=SHEET_RANGE,
                                           valueInputOption='RAW',
                                           insertDataOption='OVERWRITE',
                                           body=value_range_body).execute()
            return dict(
                success=result.get('updates', {}).get('updatedCells', 0) == 7,
                new_level=new_level
            )
        return self.bot.loop.run_in_executor(None, _add_new_offence)

    # Commands stuff
    async def cog_check(self, ctx):
        if not ctx.channel.permissions_for(ctx.author).kick_members:
            raise self.bot.SilentCheckFailure

        return True

    @commands.command(name='slowmode')
    async def slowmode_cmd(self, ctx, seconds: int):
        if seconds < 1:
            if ctx.channel.id in self.slowmode:
                del self.slowmode[ctx.channel.id]
            return

        self.slowmode[ctx.channel.id] = seconds
        self.last_sent[ctx.channel.id] = self.last_sent.get(ctx.channel.id, {})

        await ctx.message.delete()
        await ctx.send(f'Slowmode set to {seconds} seconds', delete_after=5)

    @commands.command()
    async def clear(self, ctx, n: int, user: discord.Member = None):
        """Bulk delete a series of messages"""
        if n > 500:
            return await ctx.send('For safety, the limit is 500 messages.')
        await ctx.message.delete()

        messages = []
        async for message in ctx.channel.history(limit=n):
            if user is None or message.author == user:
                if (datetime.datetime.utcnow() - message.created_at).days > 12:
                    await message.delete()
                else:
                    messages.append(message)

            if len(messages) == 100:
                await ctx.channel.delete_messages(messages)
                messages = []

        if messages:
            await ctx.channel.delete_messages(messages)

    @commands.command(aliases=['forget_memelord'])
    async def forget_punishment(self, ctx, member: commands.MemberConverter):
        """Retain a user indefinitely"""
        changed = False
        for i in list(self.in_jail):
            if i[0] == member.id:
                self.in_jail.remove(i)
                changed = True

        if changed:
            return await ctx.send(f'{member} has been removed from the local database.')
        return await ctx.send(f'{member} isn\'t *in* the local database.')

    @commands.command(aliases=['memelord', 'mute'])
    async def punish(self, ctx, member: commands.MemberConverter, length: str, *, reason: str=''):
        return await self._do_punish(ctx, member, length, reason)

    async def _do_punish(self, ctx, member, length, reason):
        """Put someone in jail"""
        unit = length[-1]
        try:
            if unit in '0123456789':
                length = int(length)
            elif unit in ['m', 'h', 'd']:
                if unit == 'm': length = int(length[:-1])
                if unit == 'h': length = int(length[:-1]) * 60
                if unit == 'd': length = int(length[:-1]) * 1440
            else:
                return await ctx.send('Expected the unit to be "m", "h" or "d".')
        except ValueError:
            return await ctx.send('Expected length to be an integer with an optional unit.')

        unique_key = uuid.uuid4().hex  # Used just to avoid collisions

        extended = False
        for i in list(self.in_jail):
            if i[1] == member.id:
                self.in_jail.remove(i)  # Keep only the most recent one
                extended = True
                if i[4]:
                    reason += f'\n{i[3]}'
                if i[3] is None:
                    length += i[2]
                else:
                    ct = time.time()
                    length += round(max(0, i[2] - (ct - i[3]) / 60))

        release_time = None
        jail = self.config.get(ctx.guild.id, {}).get('jail')
        if not jail:
            return
        if not jail.get('respond'):
            release_time = time.time() + length * 60

        convict = [ctx.guild.id, member.id, length, release_time, reason, unique_key]
        self.in_jail.append(convict)

        if member.voice:
            afk = ctx.guild.afk_channel
            await member.move_to(afk, reason='Send to jail')

        black_roles = jail.get('black_roles')
        for role in ctx.author.roles:
            if role.id in black_roles:
                await member.remove_roles(role, reason='Sent to jail')

        # APPLY ROLE
        try:
            await member.add_roles(discord.Object(jail.get('role', 0)))
        except:
            await ctx.send('Failed to apply role')

        if extended:
            await ctx.send(f'{member}\'s punishment has been extended to {length} minutes.')
        else:
            await ctx.send(f'{member} has been confined for {length} minutes.')

        if jail.get('respond'):
            chan = ctx.guild.get_channel(jail.get('channel'))
            if chan:
                await chan.send(
                    f'{member.mention} You have been muted. Please respond to see the reason and to start the timer.'
                )

    @punish.after_invoke
    @forget_punishment.after_invoke
    async def save(self, _):
        with open('state_cache/in_jail.yml', 'w') as file_:
            yaml.dump(self.in_jail, file_)

    @commands.command()
    async def hackban(self, ctx, user_id: int, *, reason=''):
        """Ban a user by ID"""
        user = discord.Object(user_id)
        try:
            await ctx.guild.ban(user, reason=reason)
            await ctx.send(f'Banned <@{user_id}>')
        except discord.Forbidden:
            await ctx.send('I do not have permissions.')
        except discord.HTTPException:
            await ctx.send('Banning failed, did you type the id correctly?')

    # Chat mods
    @commands.command()
    async def warn(self, ctx, member: commands.MemberConverter, level: int, *, reason: str):
        if not ctx.guild or ctx.guild.id != 184755239952318464:
            return

        if level not in (0, 1, 2, 3):
            return await ctx.send('The maximum level you may give is 3')

        if member.id in self.warnings:
            if self.warnings[member.id] + self.WARNING_RATELIMIT > time.time() and not ctx.guild.get_role(191344863038537728) in ctx.author.roles:
                return await ctx.send('Slow down! Contact a Moderator if you really need to do this.')

        async with ctx.typing():
            result = await self.add_new_offence(
                member.id,
                '{}#{}'.format(member.name, member.discriminator),
                level,
                reason,
                ctx.author.name + ' (via HTBote)'
            )
        self.warnings[member.id] = time.time()

        if not result['success']:
            return await ctx.send(embed=discord.Embed(
                color=0xff0000,
                title='Something went wrong!',
                description='Please contact a Moderator before attempting to rectify this problem.'
            ))

        if result['new_level'] == 5:
            try:
                await member.ban(reason=reason)
                return await ctx.send('Member was banned due to accumulated warnings')
            except discord.errors.Forbidden:
                return await ctx.send(embed=discord.Embed(
                    color=0xff0000,
                    title='Unable to ban member!',
                    description='Please contact a Moderator.'
                ))

        if result['new_level'] in (3, 4):  # Memelord
            length = {3: '1h', 4: '2h'}[result['new_level']]
            await self._do_punish(ctx, member, length, reason)

        return await ctx.send(embed=discord.Embed(
            color=0xf76c22,
            title='Warning logged',
            description='<@{}> is now at **{}**'.format(member.id, OFFENCE_TIERS.get(result['new_level'], 'Unknown'))
        ))


def setup(bot):
    global cog
    bot.add_cog(Moderation(bot))
    cog = bot.cogs['Moderation']


def teardown(bot):
    global cog
    cog.timer.close()
